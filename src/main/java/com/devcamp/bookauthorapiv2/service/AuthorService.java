package com.devcamp.bookauthorapiv2.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapiv2.model.Author;

@Service
public class AuthorService {

      // khởi tạo 6 đối tượng tác giả 
      Author tac_gia1 = new Author("thuong", "thuong@gamil.com", 'f'); // 'm'  of 'f'
      Author tac_gia2 = new Author("linh", "linh@gamil.com", 'f');

      Author tac_gia3 = new Author("duong", "duong@gamil.com", 'm');
      Author tac_gia4 = new Author("thinh", "thinh@gamil.com", 'f'); // 'm'  of 'f'

      Author tac_gia5 = new Author("trung", "trung@gamil.com", 'm'); // 'm'  of 'f'
      Author tac_gia6 = new Author("huy", "huy@gamil.com", 'm'); // 'm'  of 'f'

       // khởi tạo 3 đối tượng Arrlist ( mỗi đối tượng thêm 2 tác giả )
       
       // thêm tác giả vào Arrlist
      
       public ArrayList<Author> getArrayAuthor1(){
        ArrayList<Author> authorlist1 = new ArrayList<Author>();
        authorlist1.add(tac_gia1);
        authorlist1.add(tac_gia2);
        return authorlist1;
       }

       public ArrayList<Author> getArrayAuthor2(){
        ArrayList<Author> authorlist2 = new ArrayList<Author>();
        authorlist2.add(tac_gia3);
        authorlist2.add(tac_gia4);
        return authorlist2;
       }

       public ArrayList<Author> getArrayAuthor3(){
        ArrayList<Author> authorlist3 = new ArrayList<Author>();
        authorlist3.add(tac_gia5);
       authorlist3.add(tac_gia6);
        return authorlist3;
       }
       // hàm trả về tất cả các tác giả 
       public ArrayList<Author> getArrayAuthorAll(){
        ArrayList<Author> authorlistAll = new ArrayList<Author>();
        authorlistAll.add(tac_gia1);
        authorlistAll.add(tac_gia2);
        authorlistAll.add(tac_gia3);
        authorlistAll.add(tac_gia4);
        authorlistAll.add(tac_gia5);
        authorlistAll.add(tac_gia6);
        return authorlistAll;
       } 

       //  hàm lọc tác giả đầu vào là email
       public Author getfilterAuthorForEmail(String paramEmail){
        ArrayList<Author> authorlistAll =    getArrayAuthorAll();
        Author authorFind = new Author();
        for(Author authorElement : authorlistAll){
            if(authorElement.getEmail().equals(paramEmail)){
                authorFind = authorElement;
            }
        }
        return authorFind;
       }

       // hàm trả về  nhóm tác giả có giới tính trùng với từ khóa 
       public ArrayList<Author> getfilterAuthorForGender(char paramGender){
        ArrayList<Author> authorlistAll =    getArrayAuthorAll();
        ArrayList<Author> authorFind = new ArrayList<Author>();
        for(Author authorElement : authorlistAll){
            if(authorElement.getGender() == paramGender){
                authorFind.add(authorElement);
            }
        }
        return authorFind;
       }
    
}
